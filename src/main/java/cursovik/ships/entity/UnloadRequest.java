package cursovik.ships.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static cursovik.ships.Consts.*;

@Getter
@Setter
public class UnloadRequest {

    private Date arrivalDate;

    private String shipName;

    private String cargoType;

    private Integer cargoWeight;

    private Date unloadStarted;

    private Date unloadFinished;

    private static Map<String, Integer> unloadMultipliers = new HashMap();;

    static {
        unloadMultipliers.put(CT_BULK, 10);
        unloadMultipliers.put(CT_LIQUID, 20);
        unloadMultipliers.put(CT_CONTAINER, 30);
    }

    public int getBaseUnloadTime() throws Exception {
        Integer multiplier = unloadMultipliers.get(cargoType);
        if (multiplier == null) throw new Exception("Unknown type of cargo");

        // умножаем тонны на множитель и получаем количество минут, необходимых для разгрузки
        return cargoWeight * multiplier;

    }


}
