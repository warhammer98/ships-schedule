package cursovik.ships.entity;


import static cursovik.ships.Consts.CT_CONTAINER;

public class ContainerCrane extends Crane {

    public ContainerCrane() {
        this.cargoType = CT_CONTAINER;
    }
}
