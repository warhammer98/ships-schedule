package cursovik.ships.entity;

import java.util.Date;

public abstract class Crane {

    protected String cargoType;

    protected Boolean isBusy = false;

    protected Date busyUntil;

    public void setBusy(Date busyUntil) {
        isBusy = true;
        this.busyUntil = busyUntil;
    }

    public void setFree() {
        isBusy = false;
        this.busyUntil = null;
    }

    public String getCargoType() {
        return cargoType;
    }

    public Date getBusyUntil() {
        return busyUntil;
    }

    public Boolean isBusy() {
        return isBusy;
    }

    public Boolean isFree() {
        return !isBusy;
    }
}
