package cursovik.ships.entity;

import static cursovik.ships.Consts.CT_LIQUID;

public class LiquidCrane extends Crane {

    public LiquidCrane() {
        this.cargoType = CT_LIQUID;
    }
}
