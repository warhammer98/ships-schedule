package cursovik.ships.entity;


import static cursovik.ships.Consts.CT_BULK;

public class BulkCrane extends Crane {

    public BulkCrane() {
        this.cargoType = CT_BULK;
    }
}
