package cursovik.ships;

public class Consts {
    // cargo types
    public static final String CT_BULK = "BULK";
    public static final String CT_LIQUID = "LIQUID";
    public static final String CT_CONTAINER = "CONTAINER";

    // crane statuses
    public static final String STATUS_FREE = "FREE";
    public static final String STATUS_BUSY = "BUSY";

    // other
    public static final int CRANE_RELOAD_DELAY = 15; // время переключения крана между суднами, минуты
    public static final int FINE_PER_DAY = 1000;
    public static final String DATE_FORMAT = "dd.MM.yyyy hh:mm";
    public static final int CRANE_COST = 10000;
    public static final double STANDARD_DEVIATION = Math.sqrt(7);

}
