package cursovik.ships.http;

import cursovik.ships.entity.*;
import cursovik.ships.service.ReportGenerator;
import cursovik.ships.service.RequestsProcessor;
import lombok.Data;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static cursovik.ships.Consts.CRANE_COST;
import static cursovik.ships.Consts.STANDARD_DEVIATION;
import static cursovik.ships.Util.addDaysToDate;

@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    private RequestsProcessor requestsProcessor;

    @Autowired
    private ReportGenerator reportGenerator;

    private final Random r = new Random();

    @PostMapping
    public void processSchedule(
            @RequestParam MultipartFile file,
            @RequestParam Integer step,
            HttpServletResponse response
    ) throws Exception {

        Integer lCranes = 1;
        Integer bCranes = 1;
        Integer cCranes = 1;


        List<UnloadRequest> unloadRequests = parseSchedule(file.getInputStream());

        // рандомизируем время прибытия кораблей
        //randomizeArrivalDates(unloadRequests);

        RequestsProcessor.ProcessResult processResult = process(lCranes, bCranes, cCranes, step, new ArrayList<>(unloadRequests));

        while (true) {
            boolean c = false;

            RequestsProcessor.ProcessResult newProcessResult = process(lCranes + 1, bCranes, cCranes, step, new ArrayList<>(unloadRequests));
            if (newProcessResult.getFineSum() + newProcessResult.getCranesCost() < processResult.getFineSum() + processResult.getCranesCost()) {
                c = true;
                lCranes++;
                processResult = newProcessResult;
            }

            newProcessResult = process(lCranes, bCranes + 1, cCranes, step, new ArrayList<>(unloadRequests));
            if (newProcessResult.getFineSum() + newProcessResult.getCranesCost() < processResult.getFineSum() + processResult.getCranesCost()) {
                c = true;
                bCranes++;
                processResult = newProcessResult;
            }

            newProcessResult = process(lCranes, bCranes, cCranes + 1, step, new ArrayList<>(unloadRequests));
            if (newProcessResult.getFineSum() + newProcessResult.getCranesCost() < processResult.getFineSum() + processResult.getCranesCost()) {
                c = true;
                cCranes++;
                processResult = newProcessResult;
            }

            if (!c)
                break;
        }

        Workbook result = reportGenerator.generateReport(processResult);

        ServletOutputStream os = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\""+"result"+".xlsx\"");

        result.write(os);
        result.close();
        os.flush();
        response.flushBuffer();

        return;
    }

    private RequestsProcessor.ProcessResult process(Integer lCranes, Integer bCranes, Integer cCranes, int step, List<UnloadRequest> unloadRequests) throws Exception {
        List<Crane> cranes = new ArrayList<>();
        IntStream.range(0, lCranes).forEach(v -> cranes.add(new LiquidCrane()));
        IntStream.range(0, bCranes).forEach(v -> cranes.add(new BulkCrane()));
        IntStream.range(0, cCranes).forEach(v -> cranes.add(new ContainerCrane()));

        return requestsProcessor.process(unloadRequests, cranes, step);
    }

    private static List<UnloadRequest> parseSchedule(InputStream inputStream) throws URISyntaxException, IOException {
        Workbook workbook = new XSSFWorkbook(inputStream);

        Sheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.rowIterator();

        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(rowIterator, Spliterator.ORDERED),
                false)
                .map(row -> {
                    UnloadRequest unloadRequest = new UnloadRequest();
                    unloadRequest.setArrivalDate(row.getCell(0).getDateCellValue());
                    unloadRequest.setShipName(row.getCell(1).getStringCellValue());
                    unloadRequest.setCargoType(row.getCell(2).getStringCellValue());
                    unloadRequest.setCargoWeight((int)row.getCell(3).getNumericCellValue());

                    return unloadRequest;
                })
                .collect(Collectors.toList());
    }

    private void randomizeArrivalDates(List<UnloadRequest> requests) {
        for (UnloadRequest request: requests) {
            int scheduleDeviation = (int) Math.round(r.nextGaussian() * STANDARD_DEVIATION);
            Date arrivalDate = request.getArrivalDate();
            request.setArrivalDate(addDaysToDate(arrivalDate, scheduleDeviation));
        }
    }
}
