package cursovik.ships;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class Util {

    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, minutes);
        return c.getTime();
    }

    public static Date addDaysToDate(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    public static Duration subtractDates(Date d1, Date d2) {
        LocalDateTime ldt1 = LocalDateTime.ofInstant(d1.toInstant(), ZoneId.systemDefault());
        LocalDateTime ldt2 = LocalDateTime.ofInstant(d2.toInstant(), ZoneId.systemDefault());
        return Duration.between(ldt1, ldt2);
    }
}
