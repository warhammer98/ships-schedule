package cursovik.ships.service;

import cursovik.ships.entity.UnloadRequest;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cursovik.ships.Consts.*;
import static cursovik.ships.Util.subtractDates;

@Service
public class ReportGenerator {

    private static List<String> headers = new ArrayList<>(Arrays.asList(
            "Название судна",
            "Дата прибытия",
            "Ожидание разгрузки",
            "Дата начала разгрузки",
            "Продолжительность разгрузки"
    ));

    private static final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

    public Workbook generateReport(RequestsProcessor.ProcessResult processResult) throws Exception {
        int rowIdx = 0;

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Отчет");

        Row row = sheet.createRow(rowIdx++);

        int columnIdx = 0;
        for (String header: headers) {
            writeValue(header, row, columnIdx++);
        }

        long daysToUnloadSum = 0;
        long unloadDelaySum = 0;
        long maxUnloadDelay = Long.MIN_VALUE;
        long fineSum = 0;

        List<UnloadRequest> requests = processResult.getFinishedRequests();

        for (UnloadRequest request: requests) {
            // время ожидания начала разгрузки в минутах
            long daysToUnload = subtractDates(request.getArrivalDate(), request.getUnloadStarted()).toMinutes();
            daysToUnloadSum += daysToUnload;

            // задержка разгрузки в минутах
            long unloadDelay = subtractDates(request.getUnloadStarted(), request.getUnloadFinished()).toMinutes() - request.getBaseUnloadTime();
            unloadDelaySum += unloadDelay;
            if (unloadDelay > maxUnloadDelay) maxUnloadDelay = unloadDelay;

            fineSum += FINE_PER_DAY * Math.floor((daysToUnload + unloadDelay) / (60 * 24));

            row = sheet.createRow(rowIdx++);
            columnIdx = 0;

            writeValue(request.getShipName(), row, columnIdx++);
            writeValue(sdf.format(request.getArrivalDate()), row, columnIdx++);
            writeValue(subtractDates(request.getArrivalDate(), request.getUnloadStarted()).toDays() + " дней", row, columnIdx++);
            writeValue(sdf.format(request.getUnloadStarted()), row, columnIdx++);
            writeValue(subtractDates(request.getUnloadStarted(), request.getUnloadFinished()).toDays() + "дней", row, columnIdx);
        }

        for (int i = 0; i < headers.size(); i++) {
            sheet.autoSizeColumn(columnIdx);
        }

        Sheet resultSheet = workbook.createSheet("Результаты");

        row = resultSheet.createRow(0);
        writeValue("Число разгруженных судов", row, 0);
        writeValue(requests.size(), row, 1);

        row = resultSheet.createRow(1);
        writeValue("Среднее время ожидания в очереди", row, 0);
        writeValue(daysToUnloadSum / requests.size() / (60 * 24) + " дней", row, 1);

        row = resultSheet.createRow(2);
        writeValue("Средняя задержка разгрузки", row, 0);
        writeValue(unloadDelaySum / requests.size() / (60 * 24) + " дней", row, 1);

        row = resultSheet.createRow(3);
        writeValue("Максимальная задержка разгрузки", row, 0);
        writeValue(maxUnloadDelay / (60 * 24) + " дней", row, 1);

        row = resultSheet.createRow(4);
        writeValue("Общая сумма штрафа", row, 0);
        writeValue(fineSum + " у.е.", row, 1);

        row = resultSheet.createRow(5);
        writeValue("Краны для жидкого груза", row, 0);
        writeValue(processResult.getCranes().stream().filter(crane -> crane.getCargoType().equals(CT_LIQUID)).count(), row, 1);

        row = resultSheet.createRow(6);
        writeValue("Краны для сыпучего груза", row, 0);
        writeValue(processResult.getCranes().stream().filter(crane -> crane.getCargoType().equals(CT_BULK)).count(), row, 1);

        row = resultSheet.createRow(7);
        writeValue("Краны для контейнеров", row, 0);
        writeValue(processResult.getCranes().stream().filter(crane -> crane.getCargoType().equals(CT_CONTAINER)).count(), row, 1);

        resultSheet.autoSizeColumn(0);
        resultSheet.autoSizeColumn(1);

        return workbook;
    }

    private void writeValue(String value, Row row, int columnIdx) {
        row.createCell(columnIdx).setCellValue(value);
    }

    private void writeValue(Integer value, Row row, int columnIdx) {
        row.createCell(columnIdx).setCellValue(value);
    }

    private void writeValue(Long value, Row row, int columnIdx) {
        row.createCell(columnIdx).setCellValue(value);
    }

}
