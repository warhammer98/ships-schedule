package cursovik.ships.service;


import cursovik.ships.entity.Crane;
import cursovik.ships.entity.UnloadRequest;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static cursovik.ships.Consts.*;
import static cursovik.ships.Util.*;

@Service
public class RequestsProcessor {

    private final Random r = new Random();

    private Date currentDate;

    public ProcessResult process(List<UnloadRequest> requests, List<Crane> cranes, Integer step) throws Exception {
        List<UnloadRequest> finishedRequests = new ArrayList<>();

        long cranesCost = cranes.size() * CRANE_COST;

        // сортируем запросы по дате прибытия
        requests = requests.stream()
                .sorted(Comparator.comparing(UnloadRequest::getArrivalDate))
                .collect(Collectors.toList());

        // за текущую дату берем дату первого прибытия
        currentDate = requests.get(0).getArrivalDate();

        // пока остались неразгруженные корабли
        while (!requests.isEmpty()) {
            // обновляем статусы у кранов
            freeCranes(cranes, currentDate);

            // получаем список готовых для разгрузки кораблей
            List<UnloadRequest> arrivedShipRequests = requests.stream()
                    .filter(request -> {
                        long compareResult = request.getArrivalDate().compareTo(currentDate);
                        return compareResult <= 0;
                    })
                    .collect(Collectors.toList());

            // для каждого готового к разгрузке корабля
            for (UnloadRequest request: arrivedShipRequests) {
                // ищем свободный кран
                Crane crane = findFreeCrane(request.getCargoType(), cranes);
                // если крана нашелся
                if (crane != null) {
                    // случайная задержка окончания разгрузки, дни
                    int unloadDelayInDays = Math.abs(r.nextInt()) % 11;
                    // время разгрузки судна на основе типа груза и его веса, минуты
                    int baseUnloadTimeInMinutes = request.getBaseUnloadTime();
                    // время окончания разгрузки судна
                    Date unloadFinishedDate = addDaysToDate(
                            addMinutesToDate(currentDate, baseUnloadTimeInMinutes), unloadDelayInDays
                    );

                    // кран будет занят до окончания разгрузки судна + время на "перезарядку"
                    crane.setBusy(addMinutesToDate(unloadFinishedDate, CRANE_RELOAD_DELAY));
                    // устанавливаем время начала разгрузки
                    request.setUnloadStarted(currentDate);
                    // устанавливаем время окончания разгрузки
                    request.setUnloadFinished(unloadFinishedDate);
                    // удаляем запрос из очереди нереализованных
                    requests.remove(request);
                    // добавляем запрос в список законченных
                    finishedRequests.add(request);
                }
            }


            // добавляем одну минуту к текущему времени, при желании можно увеличить количество
            currentDate = addMinutesToDate(currentDate, step);
        }

        long daysToUnloadSum = 0;
        long unloadDelaySum = 0;
        long maxUnloadDelay = Long.MIN_VALUE;
        long fineSum = 0;

        for (UnloadRequest request: finishedRequests) {
            long daysToUnload = subtractDates(request.getArrivalDate(), request.getUnloadStarted()).toMinutes();
            daysToUnloadSum += daysToUnload;
            long unloadDelay = subtractDates(request.getUnloadStarted(), request.getUnloadFinished()).toMinutes() - request.getBaseUnloadTime();
            unloadDelaySum += unloadDelay;
            fineSum += FINE_PER_DAY * Math.floor((daysToUnload + unloadDelay) / (60 * 24));
        }

        ProcessResult result = new ProcessResult(cranesCost, fineSum, finishedRequests, cranes);

        return result;
    }

    private void freeCranes(List<Crane> cranes, Date currentDate) {
        for (Crane crane: cranes) {
            if (crane.isBusy() && currentDate.after(crane.getBusyUntil())) {
                crane.setFree();
            }
        }
    }

    private Crane findFreeCrane(String cargoType, List<Crane> cranes) {
        for (Crane crane: cranes) {
            if (cargoType.equals(crane.getCargoType()) && crane.isFree()) {
                return crane;
            }
        }

        return null;
    }

    @Data
    public static class ProcessResult {
        private Long cranesCost;
        private Long fineSum;
        private List<UnloadRequest> finishedRequests;
        private List<Crane> cranes;

        public ProcessResult(Long cranesCost, Long fineSum, List<UnloadRequest> finishedRequests, List<Crane> cranes) {
            this.cranesCost = cranesCost;
            this.fineSum = fineSum;
            this.finishedRequests = finishedRequests;
            this.cranes = cranes;
        }
    }

}
